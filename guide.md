# How to make your own x86_64 EFI image of Fittonia Linux
This may take a few hours, depending on your internet connection and your machine's performance.
First, create a directory dedicated to this build:
```sh
mkdir fittonia
cd fittonia
```

## In case you're stuck
You can contact me via:
- [Discord](https://discord.com/); my handle is `-f-r-e-d-#1889`.
- email: `royern@etu.univ-grenoble-alpes.fr`.

## Azimuth
### Rust toolchain
You need a rust toolchain to build azimuth.
Check out [rust-lang.org](https://www.rust-lang.org/tools/install) for guides about installing `rustup`.
To install the right toolchain and components:
```sh
rustup toolchain install nightly
rustup component add rustc-dev
rustup component add llvm-tools-preview
```
### Fetching the source
To download and extract Azimuth:
```sh
wget -c https://gitlab.com/fittonia/azimuth/-/archive/master/azimuth-master.tar.gz -O - | tar -xz
```

### Building Azimuth
This command will build and strip the binary:
```sh
cargo build --release --manifest-path azimuth-master/Cargo.toml
cp azimuth-master/target/release/azimuth .
strip azimuth
```
> This may take a while (between one and two hours on my machine)

## The root filesystem
### Fetching Buildroot
Buildroot is currently used to build the root filesystem.
This guide will use the february version:
```sh
wget -c https://buildroot.org/downloads/buildroot-2021.02.tar.gz -O - | tar -xz
mv buildroot* buildroot
```

### Overlay
Buildroot doesn't include keyboard layouts, and we also need somewhere to put fonts. Buildroot's "overlay" feature allows us to include arbitrary files in our root filesystem, copied from an external directory. We provide a default overlay containing all text files needed to run azimuth and fittonia.
```sh
wget -c https://cdn.discordapp.com/attachments/737276023296491610/830402296059527178/overlay.tar.gz -O - | tar -xz
```

To insert Azimuth into the root file system overlay, run this command:
```sh
mkdir -p overlay/usr/bin
cp azimuth overlay/usr/bin
```

### Glibc
We built Azimuth for a specific glibc version. It may refuse to launch if the root filesystem has a different version installed. First, find the version you need, using any of the methods listed [here](https://www.xmodulo.com/check-glibc-version-linux.html). My machine happens to have version 2.33.

To ensure the right version is installed, we need to modify `buildroot/package/glibc/glibc.mk`.
Look for these two lines in the file:
```
GLIBC_SITE = $(call github,bminor,glibc,$(GLIBC_VERSION))
endif
```
Replace them and everything above with:
```
GLIBC_VERSION = 2.33
GLIBC_SITE = http://ftp.igh.cnrs.fr/pub/gnu/libc
```
> Write your glibc version in place of '2.33'

Now, we should update the hash of the downloaded file. For now, we will simply bypass hash checks by removing a file:
```sh
rm buildroot/package/glibc/glibc.hash
```

### Buildroot configuration
We will start with this basic configuration:
```
BR2_x86_64=y
BR2_CCACHE=y
BR2_TOOLCHAIN_BUILDROOT_GLIBC=y
BR2_TOOLCHAIN_BUILDROOT_CXX=y
BR2_TARGET_GENERIC_HOSTNAME="fittonia"
BR2_TARGET_GENERIC_ISSUE="Welcome to Fittonia Linux"
BR2_ROOTFS_DEVICE_CREATION_DYNAMIC_EUDEV=y
BR2_ROOTFS_OVERLAY="../overlay"
BR2_PACKAGE_MESA3D=y
BR2_PACKAGE_MESA3D_GALLIUM_DRIVER_ETNAVIV=y
BR2_PACKAGE_MESA3D_GALLIUM_DRIVER_IRIS=y
BR2_PACKAGE_MESA3D_GALLIUM_DRIVER_KMSRO=y
BR2_PACKAGE_MESA3D_GALLIUM_DRIVER_NOUVEAU=y
BR2_PACKAGE_MESA3D_GALLIUM_DRIVER_PANFROST=y
BR2_PACKAGE_MESA3D_GALLIUM_DRIVER_R300=y
BR2_PACKAGE_MESA3D_GALLIUM_DRIVER_R600=y
BR2_PACKAGE_MESA3D_GALLIUM_DRIVER_SWRAST=y
BR2_PACKAGE_MESA3D_DRI_DRIVER_I915=y
BR2_PACKAGE_MESA3D_DRI_DRIVER_I965=y
BR2_PACKAGE_MESA3D_DRI_DRIVER_NOUVEAU=y
BR2_PACKAGE_MESA3D_DRI_DRIVER_RADEON=y
BR2_PACKAGE_MESA3D_OPENGL_EGL=y
BR2_PACKAGE_MESA3D_OPENGL_ES=y
BR2_PACKAGE_ACPID=y
BR2_PACKAGE_DBUS_GLIB=y
BR2_PACKAGE_EUDEV_RULES_GEN=y
BR2_PACKAGE_KBD=y
BR2_PACKAGE_USBUTILS=y
BR2_PACKAGE_OPENSSL=y
BR2_PACKAGE_FONTCONFIG=y
BR2_PACKAGE_HARFBUZZ=y
BR2_PACKAGE_LIBDRM_AMDGPU=y
BR2_PACKAGE_LIBDRM_VMWGFX=y
BR2_PACKAGE_LIBNL=y
BR2_PACKAGE_LIBEVDEV=y
BR2_PACKAGE_LIBUNWIND=y
BR2_PACKAGE_READLINE=y
BR2_PACKAGE_DHCPCD=y
BR2_PACKAGE_IWD=y
BR2_PACKAGE_FILE=y
BR2_PACKAGE_NANO=y
BR2_TARGET_ROOTFS_CPIO=y
# BR2_TARGET_ROOTFS_TAR is not set

```
> Notice the `BR2_ROOTFS_OVERLAY` key which specifies our overlay directory location.

Save this under `buildroot/configs/fittonia_defconfig`, then enter this command to select this configuration:
```sh
make -C buildroot fittonia_defconfig
```

### Building Buildroot
You can now begin the building of the root filesystem!
```
make -C buildroot
```
> This may take a while (about fourty minutes on my computer).

If everything went well, your root file system ended up in `buildroot/output/images/rootfs.cpio`.

## Linux kernel
We will now build the linux kernel, with the root filesystem included in the binary.

### Fetching Linux
We will use version 5.11 but you should use the one your building machine runs on.
```sh
wget -c https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.11.tar.xz -O - | tar -xJ
mv linux-* linux
```

### Base configuration
We will start from the base x86_64 config:
```sh
make -C linux x86_64_defconfig
```
Then, we will edit the configuration:
```sh
make -C linux menuconfig
```

### File size
To get the smallest kernel, we will use XZ compression. In General Setup, choose "XZ" in "kernel compression mode".
Additionally, lower in General Setup, choose "Optimize for size" in "Compiler optimization level".

### Root Filesystem
To include our root filesystem, in General Setup, enter `../buildroot/output/images/rootfs.cpio` in "Initramfs source file(s)".

### Hybrid EFI binary
To get a file we can use as an EFI program, we must enable a few options in the kernel.
In Processor types and features, enable "EFI runtime service support", "EFI stub support" and "EFI mixed-mode support".

### Quiet boot
Linux is pretty verbose during boot. To get a quiet boot, in Processor types and features, scroll to the bottom of the list, and enable "Built-in kernel command line". Then, in the option just below, enter `quiet splash`.

### GPU drivers
Azimuth renders directly to the GPU framebuffers.
For this to work, the kernel must provide specific drivers and interfaces.
First, in Device Drivers > Graphics Support, enable "Direct Rendering Manager".
Then, depending on your GPU, choose what GPU drivers you need:
- ATI Radeon
- AMD GPU
- Nouveau (NVIDIA) cards
- Intel 8xx/9xx/G3x/G4x/HD Graphics

### Quit the configuration editor
To quit menuconfig, press ESC until you see a prompt, then choose to save the configuration.

### Building Linux
It is time to build the Linux Kernel:
```sh
make -C linux -j8
```
> The 'j' option controls multithreading
> This may take a while (about twenty minutes on my computer).

## USB drive setup
Find a USB drive which has at least 100MB of capacity.

### Formatting
UEFI works on FAT partitions. FAT16 is guaranteed to work, and FAT32 works for me.
Use any utility capable of formatting USB drives, like gparted or mkfs.
Here is a [guide](https://phoenixnap.com/kb/linux-format-usb).

### EFI Setup
UEFI is so simple to setup: If for example your FAT32 partition was mounted on /mnt, just type this:
```sh
mkdir -p /mnt/EFI/BOOT/
```
Then, copy your kernel in there:
```sh
cp linux/arch/x86/boot/bzImage /mnt/EFI/BOOT/BOOTX64.EFI
```
Unmount the partition, and plug your USB drive in an x86_64 computer.

### Booting
Tell your BIOS to boot in UEFI mode on your USB drive. If you enabled quiet boot, it can take a few seconds while you should only see your motherboard manufacturer's logo on screen, and your USB drive's led going crazy. At some point, you should see the prompt: "Welcome to Fittonia Linux" as the screen switches to console mode. Linux has booted.

### Login
Enter `root` to login as root. A password is normally not required. Then, if your keyboard layout is not US, you might want to switch layout with the `loadkeys` command:
```sh
loadkeys fr
```
> This will switch the keyboard layout to french.

### Startup
To start Azimuth, enter this command:
```sh
AZ_KEYMAP="keymap.csv" AZ_FONTS="fonts/" AZ_URL="file:///root/demo.html" azimuth
```

### Specific GPU configuration
After azimuth has run once, it must have generated the `available_gpu_configs.csv` file.
This file contains one row per possible GPU configuration.
To select one, add `AZ_GPU=#` where # is the config index. For instance,
```sh
AZ_GPU="5" azimuth
```
will make azimuth use the sixth GPU configuration.

## The end
Congratulations, you have built Fittonia Linux!








